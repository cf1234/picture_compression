

$(function() {
	//将选中的图片的base64显示到身份证正面的位置
	 document.getElementById("id-face").addEventListener("change", function(){
    	$('.container').eq(0).css("background-image","url(image/UserCenter/border.png)");
        onFileChange(this,"face-result","face-empty-result")
    });
    //将选中的图片的base64显示到身份证反面的位置
    document.getElementById("id-back").addEventListener("change", function(){
    	$('.container').eq(1).css("background-image","url(image/UserCenter/border.png)");
        onFileChange(this,"back-result","back-empty-result")
    });
    
	$('.submit').on("click", function(){
    	checkFile();
    });

})

//图片改变
function imgChange(e,name,pic) {
	if(checkImg(pic,false) == false) {
		return;
	}
    var dom =$("input[id^='imgTest']")[0];
    var reader = new FileReader();
    reader.onload = (function (file) {
    })(e.target.files[0]);
    reader.readAsDataURL(e.target.files[0]);
}

//检查文件是否是图片
function checkImg(pic,open) {
        var strs = new Array(); //定义一数组
        var pic1= pic.val();
        strs = pic1.split('.');
        var suffix = strs [strs .length - 1];

        if (suffix != 'jpg' && suffix != 'gif' && suffix != 'jpeg' && suffix != 'png') {
             //这样清空，在IE8下也能执行成功
             if(open == true) {
             	toast("您选择的文件不是图片格式，请重新选择图片");
             }
           return false;
       	} else {
       		return true;
       	}
} 


/**
 * 选中图片时的处理
 * @param {*} fileObj input file元素
 * @param {*} el //选中后用于显示图片的元素ID
 * @param {*} btnel //未选中图片时显示的按钮区域ID
 */
function onFileChange(fileObj,el,btnel){
    var windowURL = window.URL || window.webkitURL;
    var dataURL;
    var imgObj = document.getElementById(el);
    document.getElementById(btnel).style.display="none";
    imgObj.style.display="block";
    if (fileObj && fileObj.files && fileObj.files[0]) {
        dataURL = windowURL.createObjectURL(fileObj.files[0]);
        imgObj.src=dataURL;
    } else {
        dataURL = fileObj.value;
        imgObj.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
        imgObj.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = dataURL;
    }
}

//提交
function submit(pic1,pic2){
	
	var formData = new FormData();
	var p = new Promise(function(resolve, reject){ 
		var faceCard = pic1[0].files[0];
		if(faceCard != null ) {
			//做一些异步操作 
			if(faceCard.size/1024 > 500) {
				photoCompress(faceCard, {
			        quality: 0.2	
			    },function(base64Codes){
			  	  	var faceCard = dataURLtoFile(base64Codes,pic1[0].files[0].name);
			  	  	resolve(faceCard);
			    });	
			}else {
			  	resolve(faceCard);
			}
		}else {
			resolve(null);
		}
	}).then(function(faceCard){
		alert(faceCard.size)
		formData.append("faceCard",faceCard);
		var face = new Promise(function(resolve, reject){ 
		//做一些异步操作 
			var backCard = pic2[0].files[0];
			if(backCard != null ) {
			//做一些异步操作 
				if(backCard.size/1024 > 500) {
					photoCompress(backCard, {
				        quality: 0.2	
				    },function(base64Codes){
				  	  	var backCard = dataURLtoFile(base64Codes,pic2[0].files[0].name);
				  	  	resolve(backCard);
				    });	
				}else {
				  	resolve(backCard);
				}
			}else {
				resolve(null);
			}
		})
		return face;
	}).then(function(backCard) {
		alert(backCard.size)
		formData.append("backCard",backCard);
//	    $.ajax({
//	        url:URLs + '/PicCompression.do?compressionImg1',
//	        type: 'POST',
//	        cache: false,
//	        data: formData,
//	        xhrFields: {
//	            withCredentials: true
//	        },
//	        processData: false,
//	        contentType: false,
//	        success:function(data){
//	        	var result = JSON.parse(data);
//	        	alert();
//	        },
//	        error:function(r){
//	        	alert("上传实名认证信息失败!");
//	        }
//	    })
	});

	
	
	
}

//验证上传的数据
function checkFile() {
	//判断图片是否有，并且是否是图片格式
	var pic1 = $("#id-face");
	var pic2 = $("#id-back");
	if(pic1.get(0).files[0] == null ) {
		alert("请上传身份证正面照");
	}else if (pic2.get(0).files[0] == null ) {
		alert("请上传身份证反面照");
	}else if(pic1.get(0).files[0] != null && checkImg(pic1,true) == false) {
		alert("请上传的身份证正面照为图片格式！");
		return;
	}else if(pic2.get(0).files[0] != null && checkImg(pic2,true) == false) {
		alert("请上传的身份证反面照为图片格式！");
		return;
	}else {
		submit(pic1,pic2);
	}
}
