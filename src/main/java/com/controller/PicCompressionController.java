package com.controller;

import com.service.PicCompressionService;
import com.utils.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

//联系我们控制层
@Controller
@RequestMapping("/PicCompressionController.do")
public class PicCompressionController {
	@Autowired
	private PicCompressionService picCompressionService;

	/**
	 * @方法名: imgComUpload
	 * @描述: 单张图片上传
	 * @param:@param imgFile
	 * @param:@throws Exception
	 * @return： String
	 * @author: chenfa
	 * @date: 2019-3-12 下午6:33:07
	 */
	@RequestMapping(params = "imgComUpload")
	@ResponseBody
	public String imgComUpload(
			@RequestParam(value = "imgFile", required = true) MultipartFile imgFile)
			throws Exception {
		ResultJson resultJson = picCompressionService.imgComUpload(imgFile);
		return resultJson.getCommonJson();
	}

	/**
	 * @方法名: imgsComUpload
	 * @描述: 多张图片上传
	 * @param:@param imgFile
	 * @param:@throws Exception
	 * @return： String
	 * @author: chenfa
	 * @date: 2019-3-12 下午6:33:07
	 */
	@RequestMapping(params = "imgsComUpload")
	@ResponseBody
	public String imgsComUpload(
			@RequestParam(value = "imgFiles", required = true) MultipartFile[] imgFile)
			throws Exception {
		ResultJson resultJson = picCompressionService.imgsComUpload(imgFile);
		return resultJson.getCommonJson();
	}

	/**
	 * @方法名: uploadImg
	 * @描述: 固定两张图片上传
	 * @param:@param imgFile
	 * @param:@throws Exception
	 * @return： String
	 * @author: chenfa
	 * @date: 2019-3-12 下午6:33:07
	 */
	@RequestMapping(params = "uploadImg")
	@ResponseBody
	public String uploadImg(
			@RequestParam(value = "faceCard", required = true) MultipartFile faceCard,
			@RequestParam(value = "backCard", required = true) MultipartFile backCard)
			throws Exception {
		ResultJson resultJson = picCompressionService.uploadImg(faceCard,
				backCard);
		return resultJson.getCommonJson();
	}
}
