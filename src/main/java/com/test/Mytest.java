package com.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mytest {

	@Autowired
	// private PhotoServiceImpl photoServiceImpl;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void test() throws Exception {

		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"classpath:applicationContext.xml",
				"file:src/main/webapp/WEB-INF/springmvc-servlet.xml");
		// photoServiceImpl = (PhotoServiceImpl)ctx.getBean("photoServiceImpl");

	}

}
