package com.service;

import org.springframework.web.multipart.MultipartFile;

import com.utils.ResultJson;

/**
 * 图片压缩上传的service层
 */
public interface PicCompressionService {

	/**
	 * @方法名: imgComUpload
	 * @描述: 单张图片上传
	 * @param:@param imgFile
	 * @param:@return
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-12 下午6:34:18
	 */
	ResultJson imgComUpload(MultipartFile imgFile);

	/**
	 * @方法名: imgsComUpload
	 * @描述: 多张图片上传
	 * @param:@param multipartFile
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-13 上午10:58:41
	 */
	ResultJson imgsComUpload(MultipartFile[] imgFiles);

	/**
	 * @方法名: uploadImg
	 * @描述: 固定两张图片上传
	 * @param:@param faceCard
	 * @param:@param backCard
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-13 上午11:39:27
	 */
	ResultJson uploadImg(MultipartFile faceCard, MultipartFile backCard);

}
