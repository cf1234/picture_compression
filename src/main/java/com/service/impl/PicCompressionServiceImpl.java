package com.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.service.PicCompressionService;
import com.utils.ResultJson;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 图片上传的serviceimpl
 * 
 */
@Service
public class PicCompressionServiceImpl implements PicCompressionService {
	Logger log = Logger.getLogger(this.getClass());

	// @Resource
	// private PicCompressionDao picCompressionDao;

	/**
	 * @方法名: imgComUpload
	 * @描述: 单张图片上传
	 * @param:@param imgFile
	 * @param:@return
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-12 下午6:34:18
	 */
	@Override
	public ResultJson imgComUpload(MultipartFile imgFile) {
		ResultJson resultJson = null;
		SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdfs.format(new Date());
		// 保存文件的路径，靠实际情况填写
		String uploadPath = converPath("C:\\Users\\OMEN\\Desktop\\picCompression\\src\\main\\webapp\\download"
				+ "\\" + date + "\\");
		// 下载文件的路径，靠实际情况填写
		// String downlooadpath="http://192.168.56.1:8080"+"/"+date+"/";
		uploadPath = converPath(uploadPath);
		int c1 = 0;
		int c2 = 0;
		if (imgFile != null) {
			saveFile(imgFile, uploadPath);
		}
		if (c1 > 0 || c2 > 0) {
			resultJson = new ResultJson("添加成功！");
		} else {
			resultJson = new ResultJson(200, "添加失败！");
		}
		return resultJson;
	}

	/**
	 * @方法名: imgsComUpload
	 * @描述: 多张图片上传
	 * @param:@param multipartFile
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-13 上午10:58:41
	 */
	@Override
	public ResultJson imgsComUpload(MultipartFile[] imgFiles) {
		SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdfs.format(new Date());
		// 保存文件的路径，靠实际情况填写
		String uploadPath = converPath("C:\\Users\\OMEN\\Desktop\\picCompression\\src\\main\\webapp\\download"
				+ "\\" + date + "\\");
		// 下载文件的路径，靠实际情况填写
		// String downlooadpath="http://192.168.56.1:8080"+"/"+date+"/";
		uploadPath = converPath(uploadPath);
		for (int i = 0; i < imgFiles.length; i++) {
			if (imgFiles[i] != null) {
				saveFile(imgFiles[i], uploadPath);
			}
		}
		return new ResultJson();
	}

	/**
	 * @方法名: uploadImg
	 * @描述: 固定两张图片上传
	 * @param:@param faceCard
	 * @param:@param backCard
	 * @return： ResultJson
	 * @author: chenfa
	 * @date: 2019-3-13 上午11:39:27
	 */
	@Override
	public ResultJson uploadImg(MultipartFile faceCard, MultipartFile backCard) {
		SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdfs.format(new Date());
		// 保存文件的路径，靠实际情况填写
		String uploadPath = converPath("C:\\Users\\OMEN\\Desktop\\picCompression\\src\\main\\webapp\\download"
				+ "\\" + date + "\\");
		// 下载文件的路径，靠实际情况填写
		// String downlooadpath="http://192.168.56.1:8080"+"/"+date+"/";
		uploadPath = converPath(uploadPath);
		if (faceCard != null) {
			saveFile(faceCard, uploadPath);
		}
		if (backCard != null) {
			saveFile(backCard, uploadPath);
		}
		return new ResultJson();
	}

	/**
	 * 
	 * @方法名：saveFile
	 * @描述: 保存文件
	 * @参数: @param request
	 * @参数: @param zpfile
	 * @参数: @param list
	 * @参数: @param uploadPath
	 * @参数: @param downlooadpath
	 * @参数: @return
	 * @返回类型: List<TbFilesUpload>
	 * @开发者： gml
	 * @创建时间: 2019-2-19 下午9:06:30
	 */
	private static void saveFile(MultipartFile zpfile, String uploadPath) {
		// TODO Auto-generated method stub

		// 获得物理路径webapp所在路径
		String pathRoot = uploadPath;
		String uploadFileName = null;// 原始文件名
		String targetFileName = null;// 重命名文件名
		String renameFileName = null;
		long fileSize = 0;// 文件大小
		String uploadFileType = null;// 文件后缀
		// 获得原始文件名
		uploadFileName = zpfile.getOriginalFilename();
		try {
			// 获取文件后缀名
			uploadFileType = uploadFileName.substring(uploadFileName
					.lastIndexOf("."));
		} catch (StringIndexOutOfBoundsException e) {
			// 获取文件后缀名
			uploadFileType = ".png";
		}

		// 重命名文件
		renameFileName = new Date().getTime() + generateRandom();
		targetFileName = renameFileName + uploadFileType;

		// 文件大小
		fileSize = zpfile.getSize();

		// 临时文件路径
		File file_normer = new File(pathRoot);
		if (!file_normer.exists()) {
			file_normer.mkdirs();
		}
		// 创建文件实例
		File tempFile = new File(pathRoot + targetFileName);// 物理路径+文件名
		try {
			zpfile.transferTo(tempFile);// 创建文件
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}

		String uploadPathStr = pathRoot + targetFileName;// 上传的路径
		String uploadFile = pathRoot + renameFileName + ".png";// 压缩后的路径
		try {
			if (fileSize / 1024 > 500) {// 判断文件的大小是否大于40k，大于压缩，小于不压缩，根据实际需求规定
				Thumbnails.of(uploadPathStr).scale(0.5f).outputQuality(1.0f)
						.toFile(uploadFile);
				uploadFileType = ".png";
				if (!uploadPathStr.equals(uploadFile)) {
					(new File(uploadPathStr)).delete();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @方法名：generateRandom
	 * @描述: 生成六位随机数
	 * @参数: @return
	 * @返回类型: String
	 * @开发者： gml
	 * @创建时间: 2018-12-5 下午3:38:48
	 */
	private static String generateRandom() {
		Random random = new Random();
		long rnd = random.nextInt(999999) % (999999 - 100000 + 1) + 100000;
		String pnum = String.valueOf(rnd);
		return pnum;
	}

	/**
	 * 
	 * @方法名：converPath
	 * @描述: 转换路径，Linux下将
	 * @参数: @param path
	 * @参数: @return
	 * @返回类型: String
	 * @开发者： gml
	 * @创建时间: 2018-12-5 下午3:48:38
	 */
	private static String converPath(String path) {
		String uploadpath = null;
		String os = System.getProperty("os.name");
		os = "linux";
		if (os.toLowerCase().startsWith("win")) {
			uploadpath = path;
		} else {
			uploadpath = path.replace("\\", "/");
		}
		return uploadpath;
	}

}
